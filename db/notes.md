# DB notes

## General
- Never use a shared database for development work

## Versioning
- My Structure
  - db/
    - baseline.sql (baseline (first version) db creation script)
    - static_data.sql (e.g. enum values script)
    - bootstrap_data.sql (initial data script)
    - deltas/
      - delta_01.sql (delta file)
      - delta_02.sql ...
      - delta_03.sql ...
