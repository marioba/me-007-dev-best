@title[Title]

# 10 <span class="gold">Lessons</span>

#### I've learned about software development
<br>
<br>
<span class="byline">in my first <span class="green">OPENGIS.ch</span> year</span>

Note:
- They came from my personal experience and from what I'm studying
- It's what I think at the moment is right for me, maybe it's is not the same for you and for your projects and is not my intention to impose anything.
- I'll start with the biggest, and the most important one.
---

@title[Requirements]
## 1. What's a good <span class="gold">requirement</span>

Note:
- I always suspected that having good requirements was a good thing, but I never really understood how to get them and especially what is a good requirement.
- Now I know what they are and how much they can make the development satisfactory.

In preparing for battle I have always found that plans are useless, but planning is indispensable.
Dwight D. Eisenhower

+++

### <span class="gold">Requirements
- A requirement is not about the solution, but about the problem to be solved
- A good requirement has a rationale and a (or more) fit criteria

```md
Requirement:  Example-1
Description:  The product shall calculate the XY coefficient of a
              recorded building
Rationale:    To be able to generate the Building Dossier
Fit criteria: The XY coefficient shall agree with the Cantonal
              Guidelines, Table 3.4
```
Note:
- These are reflections that sooner or later we need to do. Doing them early, give us all the elements to make a good design

+++
### <span class="gold">Requirements process</span>
- Conception
- Scoping
- Work investigation
- Product determination
- Requirements definition
- Analysis
- Design
- Coding

+++
### <span class="gold">Outcome</span>
- Product use cases
- Functional requirements
- Non-functional requirements
- Terminology
- Data dictionary

---
@title[Test]
## 2. <span class="gold">Testing</span> is not only about finding errors

+++

### <span class="gold">Tests</span>
- Test helps to change the code without fear even months or years after it was written
- TDD is easy and quickly if you have good requirements

---?image=keynotes/10_lessons/assets/images/deploy_qsilva.png&size=auto 90%
@title[Automate]
## 3. "Paper scripting" is not a good way to <span class="gold">automate</span> a process

Note:
- That's is not a good way to automate something
+++
### <span class="gold">Automation</span>
- Shell scripts
- Virtual environments
- Dockers
- Virtual Machines

---
@title[CI]
## 4. What is <span class="gold">C.I.</span>

+++
### <span class="gold">C.I.</span>
- Automation of building, integration tests and deployment

Note:
- Now in a new project I start with preparing Travis to execute test and deploy
- Thats means defining the git workflow early
---
@title[DB]
## 5. Always <span class="gold">version your DB</span>

Did someone say PUM?

+++
### <span class="gold">DB versioning and management</span>
- Database must be reliably reproduced in any environment
- Don't use shared DB for development

---
@title[Log]
## 6. <span class="gold">Logs</span> are your friends</span>

---
@title[Doc]
## 7. What's most important to <span class="gold">document</span>

+++
###  <span class="gold">Documentation</span>
- The rationale of the design decisions
- The development environment and how to recreate it
- The code (docstring)

---
@title[Git]
## 8. To not be afraid of <span class="gold">git</span>

+++?image=keynotes/10_lessons/assets/images/git.png&size=contain

Note:
- Learn the tool
- Define a workflow
- Follow the good practices (ex. small frequent commits)

---
@title[Code]
## 9. The importance of <span class="gold">coding conventions</span>

<span class="byline">*Any fool can write code that a computer can understand. Good programmers write code that humans can understand.*  
-Martin Fowler</span>

Note:
- Now I try to respect PEP8 etc.
- Sometimes Pythonic code is too much

+++?image=keynotes/10_lessons/assets/images/goto.png&size=contain

---
@title[Debug]
## 10. There are better <span class="gold">debuggers</span> than `print()`

+++
### <span class="gold">Debuggers</span>
- *First aid* plugin
- *pdb*
- *Pycharm* remote debugger (Pro only)

---
@title[10_commandments]
### <span class="gold">My 10 Commandments</span>

1. You shall not code without <span class="gold">requirements</span>
2. You shall <span class="gold">test</span> your code
3. You shall <span class="gold">automate</span> your processes
4. You shall <span class="gold">integrate</span> your work
5. You shall <span class="gold">version</span> the db
6. Remember to <span class="gold">log</span>
7. You shall <span class="gold">document</span> your decisions
8. Honor the good <span class="gold">git practices</span>
9. Honor the <span class="gold">code conventions</span>
10. You shall <span class="gold">use a debugger</span>


+++?image=keynotes/10_lessons/assets/images/columbo.jpg&size=contain
+++?image=keynotes/10_lessons/assets/images/scabbard_fish.jpg&size=contain

---
@title[Discussion]
## <span class="gold">Topics to discuss</span>
- Requirement workflow
- C.I. workflow
- Git workflow
- DB versioning workflow
- Logging strategies
