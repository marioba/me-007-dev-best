# Set autocompletion for QGIS, 2.18 or 3.0, in PyCharm

- Get the QGIS python path from QGIS running this code into the python console:

```python
import sys

# Uncomment in qgis2
# from __future__ import print_function

export_string = 'export PYTHONPATH=${PYTHONPATH}'

for _ in sys.path:
    export_string += ':'
    export_string + = _

print(export_string)
```

- Create a script (e.g. `pycharm-qgis.sh`) to set the correct python path first and then launch PyCharm:

```bash
#!/usr/bin/env bash

# Set the PYTHONPATH variable and call PyCharm so we can use autocompletion for QGIS

#[COPY HERE THE OUTPUT OF THE PYTHON CONSOLE]

/usr/bin/pycharm
```

- Create a Gnome application in .local/share/applications (`e.g. pycharm-qgis.desktop`) that call the script

```ini
[Desktop Entry]
Name=PyCharm QGIS
Icon=pycharm
GenericName=Python and Django IDE
Comment=Powerful Python and Django IDE. Professional version.
Exec=/home/mario/scripts/pycharm-qgis.sh
Terminal=false
Type=Application
Categories=Development;IDE;
StartupNotify=true
StartupWMClass=jetbrains-pycharm-qgis
```

## Used references
- How to setup PyCharm for QGIS development by Tudor Bărăscu
https://qtibia.ro/2016/06/09/how-to-setup-pycharm-for-qgis-development-under-linux-and-windows/ (last visit 25.03.2018)
