# QGIS remote debugging in PyCharm Professional
- Create a run configuration in PyCharm
    - Type: Python remote debug
    - Name: QGIS Debug
    - Host name: localhost
    - Port: 53100
    - Path mappings, map the source path with the plugin path in QGIS to tell PyCharm that they are the same code e.g.:
    `/home/mario/workspace/opengis.ch/my_plugin_dir/=/home/mario/.local/share/QGIS/QGIS3/profiles/default/python/plugins/my_plugin_dir`
    (QGIS 2 `/home/mario/workspace/opengis.ch/my_plugin_dir/=/home/mario/.qgis2/python/plugins/my_plugin_dir`)

- sudo pip install psutil

- Create the `startup.py` file in `~/.local/share/QGIS/QGIS3/`
(QGIS2 `~/.qgis2/python`) (it will be executed by QGIS at each startup) with the following content

```python
import psutil


def is_listening_local(port=53100):
    """Return True if someone is listening on the port"""

    els = psutil.net_connections()
    for el in els:
        if el.laddr.port == port:
            return True
    else:
        return False


if is_listening_local():
    try:
        import sys
        # Add the pydevd directory to PYTHONPATH
        sys.path.append('/opt/pycharm-professional/helpers/pydev/')

        import pydevd
        # Connect to the remote debugger
        pydevd.settrace(
            'localhost', port=53100, stdoutToServer=True, stderrToServer=True,
            suspend=False
        )
    except Exception:
        pass
```

- To debug a plugin, launch the debugger configuration and then run QGIS and it will automagically connect to the remote debugger.

## References
- Remote debugging QGIS plugins using PyCharm by Tim Sutton
http://linfiniti.com/2012/09/remote-debugging-qgis-plugins-using-pycharm/ not available yet. Copy on https://planet.qgis.org/planet/tag/pycharm/ (25.03.2018)
- QGIS Python Programming Cookbook Second edition
- Mastering QGIS Second edition
- https://github.com/fabioz/PyDev.Debugger/issues/94
