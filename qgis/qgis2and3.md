
# Use QGIS2 and QGIS3 on Archlinux

This is a hack waiting for the moment that qgis2 and qgis3 could be installed simultaneously on the same machine.

- Generate the package for qgis-ltr (2.18.18) from AUR with yaourt (say no at the installation step):
`yaourt -Sb --export . qgis-ltr`
- Generate the package for qgis (3.0.1) from AUR with yaourt (say no at the installation step):
`yaourt -Sb --export . qgis`
- Copy the generated packages (files .pkg.tar.xz) to `/opt/qgis_pkg/`
- Create script (e.g. `install_run_qgis2.sh`) for install qgis-ltr from local package file:

```bash
#!/usr/bin/env bash

# Script to run qgis-ltr (2.18)

# Verify if qgis-ltr is installed
if pacman -Qs '^qgis-ltr'; then
  # launch qgis-ltr
  /usr/bin/qgis
else
  # remove installed qgis
  sudo pacman --noconfirm -R qgis
  # install from local package and launch
  sudo pacman --noconfirm -U /opt/qgis_pkg/qgis-ltr-2.18.18-1-x86_64.pkg.tar.xz
  /usr/bin/qgis
fi
```

- Create script (e.g. `install_run_qgis3.sh`) for install qgis from local package file:

```bash
#!/usr/bin/env bash

# Script to run qgis (3.0)

# Verify if qgis is installed
if pacman -Qs '^qgis 3'; then
  # launch qgis
  /usr/bin/qgis
else
  # remove installed qgis
  sudo pacman --noconfirm -R qgis-ltr
  # install from local package and launch
  sudo pacman --noconfirm -U /opt/qgis_pkg/qgis-3.0.1-1-x86_64.pkg.tar.xz
  /usr/bin/qgis
fi
```

- Create Gnome apps to launch the scripts

`.local/share/applications/qgis2.desktop`

```ini
[Desktop Entry]
Name=QGIS2
Icon=qgis
GenericName=Geographic Information System
Comment=QGIS LTR 2.18
Exec=/home/mario/scripts/install_run_qgis2.sh
Terminal=false
Type=Application
Categories=Qt;Education;Science;Geography;
StartupNotify=true
```

`.local/share/applications/qgis3.desktop`

```ini
[Desktop Entry]
Name=QGIS3
Icon=qgis
GenericName=Geographic Information System
Comment=QGIS 3.0
Exec=/home/mario/scripts/install_run_qgis3.sh
Terminal=false
Type=Application
Categories=Qt;Education;Science;Geography;
StartupNotify=true
```

Passing from one to the other, takes about 5-10s

## Comment 20180926
I had to remove `qca` from conflicts/replaces/provides of the PKGBUILD of `aur/qca-qt4` in order to install both.
