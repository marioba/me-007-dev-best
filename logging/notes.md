# Logging

## General
- Common log levels
    - Debug
      - Enabled only when we are debugging, used for big data or high frequency data
    - Info
      - Used for routines, for example, handling requests or server state changed.
    - Warning
      - Used when it is important, but not an error, for example, when a user attempts to login with wrong password or connection is slow
    - Error
      - Used when something is wrong, for example, an exception is thrown, IO operation failure or connectivity issue
    - Critical
      - Used seldom, you can use it when something really bad happen, for example, out of memory, disk is full or a nuclear meltdown
- In Python use the logger name as `__name__`, so the message is logged with the current module name.
- In Python, use `RotatingFileHandler` instead of `FileHandler` to use rotation on log files

## Exception
- To capture exceptions with traceback, in Python:  

```python
try:
    open('/path/to/does/not/exist', 'rb')
except (SystemExit, KeyboardInterrupt):
    raise
except Exception, e:
    logger.error('Failed to open file', exc_info=True)
```

- By calling logger methods with `exc_info=True` parameter, traceback is dumped to the logger (same as `logger.exception(msg, *args)`)
