# Docker notes

## General
- A docker image is like a git repo, a docker container is the "implemetation" of the image
- A docker image can be:
  - Base image, without parents (e.g. ubuntu, busybox, debian)
  - Child image, build on base image
- Docker images are:
  - Official images, maintained by docker, without the "repo/" in the name (e.g. python)
  - User images, created by users e.g. (user/imagename)
