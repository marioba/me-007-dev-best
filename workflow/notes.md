# Workflow notes

## Standalone projects

- Long-running branches
    - `master`
    - `review` (or demo?)

- Each new feature, bugfix, etc. is a new branch created from `master` with the name starting with the issue number. No need to push if I'm the only one developing this branch. And the issue goes to *In Progress* on Zenhub
- When a new feature is complete and ready to go in production, the branch is merged in `master` and the issue on Zenhub is moved to *Closed*
- When a new feature is complete and need a feedback from someone, is merged into `review` and the issue on Zenhub is moved to *Review/QA*
- When a feature in `review` is accepted, it is merged into `master`
- The `review` branch is deleted and recreated each time from `master` (is a problem for Travis?) or merged with `master`
- Travis creates and deploys a release on each push on `master` on the production server
- Travis creates and deploys a release on each push on `review` on the demo server

## Third part/forked projects (QGIS, ili2db, ...)
- `master` (in my forked repo) means ready to PR
