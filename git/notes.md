# Git notes

## General
- The HEAD is the active/checked out branch
- Test before you commit
- If your local branch contains commits that haven't been published / pushed to the remote repository, your local branch is "ahead" of its remote counterpart branch by some commits.
- If your teammates, on their part, have uploaded commits to the remote, the remote branch will have commits that you haven't downloaded / pulled to your local branch, yet. Your local branch is then "behind" its remote counterpart branch.
- 'HEAD detached' means that we have checked out a specific commit (not the tip of the branch). Normally should be avoided (exception in the submodules when is normal to be in 'HEAD detached' state. If we want to commit something, first we need to checkout a branch)

## Commit
- Commit related changes
- Commit often
- Don't commit half-done work
- Message
    - First line: summary (50 char max.)
    - Second line: blank
    - Then
      - Motivation for the change
      - What was changed

## Squash more commits into one
- `git rebase -i HEAD~2 (num of commits)` + `git push --force`
- Or from another branch (e.g. master), `git merge --squash branch` will add the files from the other branch to the current branch to be committed

## Stash
- Name the stashes with `git stash save`

## Branch
- Create a new branch on each new topic (feature, bugfix, experiment, alternative, ...)
- If we fetched a new remote branch, `git checkout --track branch-name` will checkout the branch and establish a tracking with the remote (like the `git push -u`)
- Copy a file from another branch to the current `git checkout otherbranch myfile.txt`
- We can divide branches into two categories:
    - Short-Lived / Topic Branches
    - Long-Running Branches (e.g. test, development, ecc.)
        - Don't work on them directly, but integrate other branches into them

## Fetch & pull
- Fetch don't touch any of the local branches or the files in the working copy. It just downloads data from the specified remote and makes it visible.
- `git fetch origin` (or any other remote) + `git log origin/master` to inspect new commit fetched from origin before integrating them into my working tree
- `git pull` = `git fetch` + `git merge`. It downloads data into local repository and integrates them into the working tree

## Undo
- Fix the last commit (only the last one and only if not published):
    - `git commit --amend -m "This is the correct message"`
    - or `git add some/changed/file.ext`+ `git commit --amend -m "commit message"`
- Restore a file to its last committed version
    - `git checkout HEAD file/to/restore.ext`
- Discard all current local changes
    - `git reset --hard HEAD`
- Revert a commit on the current HEAD branch, creating a new commit that revert the selected one
    - `git revert 2b504be` (hash of the commit to revert)
- Reset the current HEAD branch to an older revision
    - `git reset --hard 2be18d9` (hash of the commit to which reset)
    - `git reset --keep 2be18d9` same as above, but the deleted changes are preserved as local changes

## Diff
- Normally the file A (-) is the old, the file B (+) is the new one.
- We can ask the diff of two branches `git diff master..branch_1` or two arbitrary revisions `git diff 0023cdd..fcd6199`

## Merge & conflicts
- The target of a merge is always the current HEAD branch
- Conflicts are only local
- It's always possible to undo a merge
- Conflicted files
    - Lines from `<<<<<<< HEAD` to `=======` are from the current working branch (HEAD)
    - Lines from `=======` to `>>>>>>> [other/branch/name]` are from the other branch
    - At the end of the cleaning, the file should look exactly as we want it to look.
- It's possible to define a tool to manage merging in `git config` and invoke it with `git mergetool` when needed
- After the conflict is solved, we commit
- `git merge --abort` return to the state before starting the merge

## Rebase
- Rebasing is similar to merging but behaves a little differently. In a merge, if both branches have changes, then a new merge commit is created. In rebasing, Git will take the commits from one branch and replay them, one at a time, on the top of the other branch.
- Like `git merge` but without a merge commit automatically created by git
- A `git rebase` will apply the commits done on the other branch just after the common ancestor and then apply the commits of out branch on the top of them
- `git rebase` rewrites history. It's to be used only for cleaning up local work, never to rebase commit that have already been published

## Submodules
- Only the submodule's remote URL, local path and checked out revision are stored by the main repository
- `git clone` by default only downloads the project itself not the submodules, to solve this we can:
    - `git clone --recurse-submodules` or
    - `git submodule update --init --recursive`
- Submodules always point to a specific commit, not a branch
- All git commands performed into the submodule context (i.e. into the submodule directory), they will be executed on the submodule, not on the parent repository
- We can point the submodule to a specific revision (commit or tag) by doing `git checkout commit_hash` into the submodule and verify with `git submodule status` (the '+' before the hash code in the status result, means that the pointed revision is not the one we currently have checked out in our project)
    - To check out the pointed revision we do `git submodule update path_of_the_submodule` (git submodule update will first fetch the submodule and then check out)
- Remove a submdule with `git submodule deinit` + `git rm` to remove the actual submodule files

## Git-flow
- Is a set of scripts that helps to have a workflow with
    - A long-running master branch
    - A long-running develop branch
    - Short-running feature branches build on top of develop
    - Short-running hotfix branches build on top of master

## LFS (Large File Storage)
- System to have only the pointer of a big file stored on a LFS store instead of into the repository
